/*
 ==============================================================================
 
 MainComponent.h
 Created: 28 Sep 2013 12:15:40pm
 Author:  Martin Robinson
 
 ==============================================================================
 */

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "../../../common/FMODHeaders.h"

class MainContentComponent : public Component,
public ButtonListener,
public SliderListener,
public Timer
{
private:
  // FMOD objects
  Studio::System* system;
  Studio::EventInstance* eventInstance;
  //    Studio::CueInstance* noteoffCue;
  Studio::ParameterInstance* paramInstance;
  
  // Juce objects
  ToggleButton startStopButton;
  Slider excitementSlider;
  
public:
  MainContentComponent()
  {
    // set up the button
    startStopButton.setButtonText ("Start/Stop");
    addAndMakeVisible (&startStopButton);
    startStopButton.addListener (this);
    
    // set up a Slider, this has some simlarities to buttons
    addAndMakeVisible (&excitementSlider);
    
    // set the range of the slider, - this is 0.0-0.1 in steps of 0.01
    excitementSlider.setRange (0.0, 1.0, 0.01);
    
    // Sliders have listeners too, notice this MainComponent is a SliderListener
    excitementSlider.addListener (this);
    
    initFMODStudio();
    startTimer (15); // call timerCallback() every 15 ms
    
    setSize (300, 200);
  }
  
  ~MainContentComponent()
  {
    stopTimer();
    shutdownFMODStudio();
  }
  
  void initFMODStudio()
  {
    // Find the common resources path on this platform
    File resourcesPath = getResourcesPath();
    
    // Get the bank file path
    File bankPath = resourcesPath.getChildFile ("MasterBank.bank");
    File stringsPath = resourcesPath.getChildFile ("MasterBank.strings.bank");
    
    // Open the FMOD System
    
    ERRCHECK (Studio::System::create (&system));
    ERRCHECK (system->initialize (64, FMOD_STUDIO_INIT_LIVEUPDATE,
                                  FMOD_INIT_PROFILE_ENABLE, 0)); // extraDriverData = 0
    
    // Load the bank into the system
    Studio::Bank* bank;
    ERRCHECK (system->loadBankFile (bankPath.getFullPathName().toUTF8(),
                                    FMOD_STUDIO_LOAD_BANK_NORMAL,
                                    &bank));
    Studio::Bank* stringsBank;
    ERRCHECK (system->loadBankFile (stringsPath.getFullPathName().toUTF8(),
                                    FMOD_STUDIO_LOAD_BANK_NORMAL,
                                    &stringsBank));
    
    // Get the event desctription
    Studio::EventDescription* eventDescription;
    ERRCHECK (system->getEvent ("event:/root/crowdahdsr",
                                &eventDescription));
    
    // Make an event instance from the event description
    ERRCHECK (eventDescription->createInstance (&eventInstance));
    
    // Get our excitement parameter
    ERRCHECK (eventInstance->getParameter ("excitement", &paramInstance));
  }
  
  void shutdownFMODStudio()
  {
    ERRCHECK (eventInstance->release());
  }
  
  void resized()
  {
    // position the components
    startStopButton.setBounds (10, 30, getWidth()-20, 20);
    excitementSlider.setBounds (10, 60, getWidth()-20, 20);
  }
  
  void buttonClicked (Button* button)
  {
    if (&startStopButton == button)
    {
      // we know that the button has changed but we need to determine
      // whether the button is now ON or OFF
      const bool isPlaying = startStopButton.getToggleState();
      
      // respond appropriately
      if (isPlaying)
      {
        ERRCHECK (eventInstance->start());
      }
      else
      {
        // trigger the queue point and fade out
        ERRCHECK (eventInstance->triggerCue());
        ERRCHECK (eventInstance->stop (FMOD_STUDIO_STOP_ALLOWFADEOUT));
      }
    }
  }
  
  void sliderValueChanged (Slider* slider)
  {
    // deciphering sliders is similar to buttons
    if (&excitementSlider == slider)
    {
      // get the value from the slider
      const float excitement = (float)excitementSlider.getValue();
      
      // use the value as the excitement parameter value for our event
      ERRCHECK (paramInstance->setValue (excitement));
    }
  }
  
  void timerCallback()
  {
    ERRCHECK (system->update());
  }
};

#endif  // MAINCOMPONENT_H_INCLUDED
